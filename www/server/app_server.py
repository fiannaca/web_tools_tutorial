#! /usr/bin/env python

import cherrypy

class AppServer(object):

	def __init__(self):
		self.usrname = 'fiannaca'
		self.rootDir = '/home/' + self.usrname + '/catkin_ws/src/web_tools_tutorial/www/'

	@cherrypy.expose
	def index(self):
		return open(self.rootDir + 'index.html')

if __name__ == '__main__':
	cherrypy.config.update({'server.socket_host': '0.0.0.0'})
	cherrypy.quickstart(AppServer(), '/', 'app_server.conf' )
