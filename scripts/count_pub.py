#!/usr/bin/env python

import rospy
from std_msgs.msg import String

def count_pub():
    pub = rospy.Publisher('count_msg_a', String)

    rospy.init_node('count_pub', anonymous=True)

    rate = rospy.Rate(1) 
    count = 0

    while not rospy.is_shutdown():
        count_msg = str(count)
        count += 1

        rospy.loginfo(count_msg)
        pub.publish(count_msg)

        rate.sleep()

if __name__ == '__main__':
    try:
        count_pub()
    except rospy.ROSInterruptException:
        pass